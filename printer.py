from PyQt5.QtWidgets import QMainWindow, QApplication
import sys

from ui_printer import Ui_Printer


class Printer(QMainWindow):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.ui = Ui_Printer()
        self.ui.setupUi(self)

    def load_image(self):
        pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    printer = Printer()
    printer.show()
    sys.exit(app.exec_())
