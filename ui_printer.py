# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Printer\printer.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Printer(object):
    def setupUi(self, Printer):
        Printer.setObjectName("Printer")
        Printer.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(Printer)
        self.centralwidget.setObjectName("centralwidget")
        Printer.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Printer)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuAbout = QtWidgets.QMenu(self.menubar)
        self.menuAbout.setObjectName("menuAbout")
        Printer.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(Printer)
        self.statusbar.setObjectName("statusbar")
        Printer.setStatusBar(self.statusbar)
        self.actionImage = QtWidgets.QAction(Printer)
        self.actionImage.setObjectName("actionImage")
        self.actionQuit = QtWidgets.QAction(Printer)
        self.actionQuit.setObjectName("actionQuit")
        self.menuFile.addAction(self.actionImage)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionQuit)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuAbout.menuAction())

        self.retranslateUi(Printer)
        self.actionQuit.triggered.connect(Printer.close)
        QtCore.QMetaObject.connectSlotsByName(Printer)

    def retranslateUi(self, Printer):
        _translate = QtCore.QCoreApplication.translate
        Printer.setWindowTitle(_translate("Printer", "MainWindow"))
        self.menuFile.setTitle(_translate("Printer", "File"))
        self.menuAbout.setTitle(_translate("Printer", "About"))
        self.actionImage.setText(_translate("Printer", "Image"))
        self.actionQuit.setText(_translate("Printer", "Quit"))

